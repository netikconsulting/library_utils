/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var config = require('./config');
var moment = require('moment-timezone');
var properties = require('tea-properties');

var utils = {};

utils.allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {res.status(200).send('OK');}
    else {next();}
};

utils.applyCacheCb = function (obj, key, cache, cacheKey){
  var original = obj[key];
  if (!original) return;
  if (!cacheKey) { cacheKey = defaultCacheKey;}
  var cached = function(){
    var cArguments = arguments;
    cacheKey(cArguments, function (err,cKey){
      if (err||!cKey){console.log("Error cKey:",err);orginal.apply(obj,cArguments);}
      var entity = cache.get(cKey);
      var cb = cArguments[cArguments.length-1];
      if (entity) { console.log("CACHE HIT",cache.label, key, cKey);return cb(null, entity);}
      // We remove the last parameter, assuming it's a callback
      var newArgs = Array.prototype.slice.call(cArguments,0,-1);
      console.log("CACHE FAIL",cache.label, key, cKey);
      newArgs.push(function(err, entity){
        if (!err && entity){cache.set(cKey, entity);}
        cb(err, entity);
      });
      original.apply(obj,newArgs);
    });
  };
  obj[key] = cached;
};

utils.expireCacheCb = function (obj, key, cache, cacheKey){
  var original = obj[key];
  if (!original) return;
  if (!cacheKey) { cacheKey = defaultCacheKey;}
  var cached = function(){
    var cArguments = arguments;
    cacheKey(arguments, function (err,cKey){
      if (err||!cKey){console.log("Error cKey:",err);original.apply(obj, cArguments);}
      var callBackOrig = cArguments[cArguments.length-1];
      var callBackNew = function(){
        cache.del(cKey);
        console.log("CACHE REMOVED", key, cKey);
        callBackOrig.apply(obj, arguments);
      };
      cArguments[cArguments.length-1] = callBackNew;
      original.apply(obj, cArguments);
    });
  };
  obj[key] = cached;
};

var defaultCacheKey = function(args, cb){
   cb(null,JSON.stringify(Array.prototype.slice.call(args,0,-1)));
};

/**
 * Deep copy an object (make copies of all its object properties, sub-properties, etc.)
 * An improved version of http://keithdevens.com/weblog/archive/2007/Jun/07/javascript.clone
 * that doesn't break if the constructor has required parameters
 * 
 * It also borrows some code from http://stackoverflow.com/a/11621004/560114
 */
var deepcopy = function deepCopy(src, /* INTERNAL */ _visited) {
    if(!src || typeof(src) !== 'object'){
        return src;
    }

    // Initialize the visited objects array if needed
    // This is used to detect cyclic references
    if (!_visited){
        _visited = [];
    }
    // Otherwise, ensure src has not already been visited
    else {
        var i, len = _visited.length;
        for (i = 0; i < len; i++) {
            // If src was already visited, don't try to copy it, just return the reference
            if (src === _visited[i]) {
                return src;
            }
        }
    }

    // Add this object to the visited array
    _visited.push(src);

    //Honor native/custom clone methods
    if(typeof src.clone == 'function'){
        return src.clone(true);
    }

    //Special cases:
    //Array
    if (Object.prototype.toString.call(src) == '[object Array]') {
        //[].slice(0) would soft clone
        ret = src.slice();
        var i = ret.length;
        while (i--){
            ret[i] = deepCopy(ret[i], _visited);
        }
        return ret;
    }
    //Date
    if (src instanceof Date){
        return new Date(src.getTime());
    }
    //RegExp
    if(src instanceof RegExp){
        return new RegExp(src);
    }
    //DOM Elements
    if(src.nodeType && typeof src.cloneNode == 'function'){
        return src.cloneNode(true);
    }

    //If we've reached here, we have a regular object, array, or function

    //make sure the returned object has the same prototype as the original
    var proto = (Object.getPrototypeOf ? Object.getPrototypeOf(src): src.__proto__);
    if (!proto) {
        proto = src.constructor.prototype; //this line would probably only be reached by very old browsers 
    }
    var ret = object_create(proto);

    for(var key in src){
        //Note: this does NOT preserve ES5 property attributes like 'writable', 'enumerable', etc.
        //For an example of how this could be modified to do so, see the singleMixin() function
        ret[key] = deepCopy(src[key], _visited);
    }
    return ret;
};

//If Object.create isn't already defined, we just do the simple shim, without the second argument,
//since that's all we need here
var object_create = Object.create;
if (typeof object_create !== 'function') {
    object_create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

utils.deepcopy = deepcopy;

var toString = Object.prototype.toString;

utils.isArray = Array.isArray /* || function(obj) {
  return toString.call(obj) == '[object Array]';
  }*/;

utils.isObject = function(obj) {
  return obj === Object(obj);
};

utils.isFunction = function(obj) {
  return toString.call(obj) == '[object Function]';
};

utils.isNumber = function isNumber(o) {
    return typeof o == "number" || (typeof o == "object" && o['constructor'] === Number);
};

utils.isString = function isString(o) {
    return typeof o == "string" || (typeof o == "object" && o['constructor'] === String);
};

utils.type = (function(global) {
    var cache = {};
    return function(obj) {
        var key;
        return obj === null ? 'null' // null
            : obj === global ? 'global' // window in browser or global in nodejs
            : (key = typeof obj) !== 'object' ? key // basic: string, boolean, number, undefined, function
            : obj.nodeType ? 'object' // DOM element
            : cache[key = ({}).toString.call(obj)] // cached. date, regexp, error, object, array, math
            || (cache[key] = key.slice(8, -1).toLowerCase()); // get XXXX from [object XXXX], and cache it
    };
  }(this));

var existsFieldOne=function(data,field){
  return data[field] !==undefined;
};

utils.exitsField=function(data,fields){
  if (!data || !fields){return;}
  if (!utils.isArray(fields)){return existsFieldOne(data,fields);}
  for (var i = fields.length - 1; i >= 0; i--) {
    var key = fields[i];
    if (existsFieldOne(data,key)){return key;}
  }
  // search prefixes
  for (var f = 0; i < fields.length; i++) {
    var field = fields[i];
    for (var k in data){
      if (utils.beginsWith(f, k)){return k;}
    }
  }
  return;
};

utils.existFieldsAll=function(data,fields){
  if (!data || !fields){return;}
  if (!utils.isArray(fields)){return existsFieldOne(data,fields);}
  for (var i = fields.length - 1; i >= 0; i--) {
    var key = fields[i];
    if (!existsFieldOne(data,key)){return false;}
  }
  return true;
};

utils.sortByKey = function(array, key, reverse) {
  return array.sort(function(a, b) {
    var x = a[key];
    var y = b[key];
    if (typeof x == "string") {
      x = x.toLowerCase();
      y = y.toLowerCase();
    }
    if (reverse){var z=x; x=y; y=z;}
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  });
};

utils.excelDate = function(value, timezone){
  var date = moment.unix((value-25569)*86400);
  // Excel asume que las fechas que lleva son GMT. Pero los usuarios las ponen con GMT Europe/Madrid
  var sdate = date.tz("GMT").format("DD-MM-YY HH:mm")+" "+date.tz(timezone).format("Z");
  return moment(sdate,"DD-MM-YY HH:mm Z");
};

/**
 * Retrieve nested item from object/array
 * @param {Object|Array} obj
 * @param {String} path dot separated
 * @param {*} def default value ( if result undefined )
 * @returns {*}
 */
utils.path = function(obj, path, def){
    var i, len;

    for(i = 0,path = path.split('.'), len = path.length; i < len; i++){
        if(!obj || typeof obj !== 'object') return def;
        obj = obj[path[i]];
    }

    if(obj === undefined) return def;
    return obj;
};

utils.applyCommandLineConfiguration = function(data, config){
  if (!config){return data;}
  if (!utils.isArray(config)){ config= [config];}
  for (var i = 0; i < config.length; i++) {
    var c = config[i].split("=");
    if (c.length!=2){console.log("Opcion de configuración inválida:",config[i]);continue;}
    var key = c[0]; var value = c[1];
    properties.set(data, key, value);
  }
  return data;
};

utils.applyChangeset = function(entity, changeset){
  for(var k in changeset){
    properties.set(entity, k, changeset[k]);
  }
};

utils.getNestedProperty = function(entity, property){
  return properties.get(entity, property);
};

utils.isEmpty = function(obj) {
  for(var i in obj) { return false; }
  return true;
};

utils.beginsWith = function(needle, haystack){
    return (haystack.substr(0, needle.length) == needle);
};

utils.round = function(value, k){
  var decimals = k;
  if (utils.isString(value)){value = parseFloat(value);}
  return +value.toFixed(decimals);
};

utils.kyotoCO2 = function(energy){
  return energy*0.65;
};

/*
String.prototype.hashCode = function(){
  var hash = 0;
  if (!this.length){return hash;}
  for (i = 0; i < this.length; i++) {
    char = this.charCodeAt(i);
    hash = ((hash<<5)-hash)+char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
};
*/

module.exports = utils;
module.exports.readConfig = config.readConfig;
module.exports.initDB = config.initDB;

var errors = require('./errors');
module.exports.addErrorMessage = errors.addMessage;
module.exports.getErrorMessage = errors.getMessage;
module.exports.formatErrorMessage = errors.formatMessage;
module.exports.handleErr = errors.handleErr;
module.exports.handleKeyErr = errors.handleKeyErr;
