var utils = require('./index');
var util = require('util');

var errorData = {};

var api = {};

api.addMessage = function(key,data){
  data.subcode = key;
  api[key]=data;
};

api.getMessage = function(key){
  if (!api[key]){
    console.log("Unknown message key",key);
    key='unknown';
  }
  return utils.deepcopy(api[key]);
};

api.formatMessage = function(key,data){
  var error = api.getMessage(key);
  if (data){
    error.parameters = data;
    var tmp = [error.msg];
    if (utils.isArray(data)){
       tmp.push.apply(tmp,data);
    } else {
      tmp.push(data);
    }
    error.msg = util.format.apply(util,tmp);
  }
  return error;
};

api.handleErr = function(err, res, req){
  if (!err) { return res.status(200).send("OK");}
  if (req && req.log){
    req.log.warn(err);
  } else {
    console.log({err:err},"handleErr");
  }
  if (!utils.isObject(err)) {return res.status(500).json({status:500,code:"INTERNAL",msg:err});}
  var status = err.status||500;
  var msg = err.msg||'Unexpected error';
  return res.status(status).json(err);
};

api.handleKeyErr = function(key, data, res, req){
  if (arguments.length == 2){
    res = data;
    data = null;
  }
  return api.handleErr(api.formatMessage(key,data), res, req);
};


api.addMessage("unknown",{status:500,code:"INTERNAL",msg:"Unexpected error"});
api.addMessage("noEntityUpdated",{status:500,code:"INTERNAL",msg:"No entity was updated or removed"});
// GENERAL PARAMETER
api.addMessage("invalidDate",{status:400,code:"BAD_PARAMETER",msg:'Invalid date format'});
api.addMessage("requiredParameterOr",{status:400,code:"MISSING_PARAMETER",msg:'Value for one of the parameters "%s" or "%s" is required'});
api.addMessage("requiredParameterExclusiveOr",{status:400,code:"BAD_PARAMETER",msg:'Value for only one of the parameters "%s" or "%s" is required'});
api.addMessage("positivePeriod",{status:400,code:"BAD_PARAMETER",msg:'Parameter must be greater than 0'});
api.addMessage("mustArray",{status:400,code:"BAD_PARAMETER",msg:'Parameter must be an array'});
api.addMessage("requiredParameter",{status:400,code:"MISSING_PARAMETER",msg:'Parameter is required'});
api.addMessage("enumInvalidParameter",{status:400,code:"BAD_PARAMETER",msg:'Parameter has invalid value'});
api.addMessage("forbiddenParameter",{status:403,code:"BAD_PARAMETER",msg:'Request has forbidden fields'});
api.addMessage("requiredMinValueParameter",{status:400,code:"BAD_PARAMETER",msg:'Parameter "%s" must be greater than %s'});
// QSENSING
api.addMessage("incorrectField",{status:400,code:"BAD_PARAMETER",msg:'Fields must be space separated. Field "%s" is not correct. Correct values are [%s]'});
// AUTH
api.addMessage("auth:invalidTokenPurpose",{status:403,code:"BAD_TOKEN",msg:'Invalid Token Purpose'});
api.addMessage("auth:invalidTokenLocation",{status:403,code:"BAD_TOKEN",msg:'Invalid Token Location'});
api.addMessage("auth:tokenOnce",{status:401,code:"BAD_TOKEN",msg:'This token can only be used once and it has already been used'});
api.addMessage("auth:badToken",{status:401,code:"BAD_TOKEN",msg:'The token is not valid'});
api.addMessage("auth:staleToken",{status:401,code:"BAD_TOKEN",msg:'Original user credentials have changed'});
api.addMessage("auth:expiredToken",{status:401,code:"BAD_TOKEN",msg:'The token has expired'});
api.addMessage("auth:entityUnknown",{status:404,code:"BAD_PARAMETER",msg:'Unknown entity'});
// PRODUCTION
api.addMessage("prod:tooFewData",{status:400,code:"BAD_PARAMETER",msg:'Not enough production data in interval'});
api.addMessage("prod:unknownRegressionModel",{status:400,code:"BAD_PARAMETER",msg:'Unknown regression model'});
api.addMessage("prod:invalidXSLXFile",{status:400,code:"BAD_PARAMETER",msg:'Invalid XSLX file'});
api.addMessage("prod:invalidXSLXTemplate",{status:400,code:"BAD_PARAMETER",msg:'File generated with invalid template'});
api.addMessage("prod:incompatibleProductionInterval",{status:400,code:"BAD_PARAMETER",msg:'Incompatible production interval with device configuration'});
// DEVICE
api.addMessage("dev:requiredArray3",{status:400,code:"BAD_PARAMETER",msg:'Parameter must be an array of three numbers'});
module.exports = api;
