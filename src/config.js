/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var fs = require("fs");

var readConfig = function(cb){
  var path =__dirname.split('/node_modules/')[0]+'/config/config.json';
  var data = null;
  fs.readFile(path, 'utf8', function(err, data){
    if (err) {return cb(err);}
    data = JSON.parse(data);
    cb(null,data);
  });
};

var initDB = function(data, cb){
  var dbManagerName = data.dbManager;
  var dbManager = require(dbManagerName);
  dbManager.init(data, function (err){
    if (err){cb(err);}
    cb(null,dbManager.models);
  });
};



module.exports.readConfig = readConfig;
module.exports.initDB = initDB;
